//"#5aabe0" // blue
//"#d6c41c" //yellow
//"#28bf65" //green
var canvas, ctx, canvasWidth, canvasHeight, updateInterval, playerSpawnX, playerSpawnY, GRAVITY = 0.2, player,
    TUNEL_HEIGHT = 200, currentGameState;
var bottomPipes = [], topPipes = [];

//INPUTS
var INPUT = {
    space: 32
};

var GAME_STATES = {
    notStarted: 0,
    playing: 1,
    gameOver: 2,
    victory: 3
};

document.addEventListener("keydown", handleInput);

var player = {
    radius: 25,
    color: "#5aabe0", // blue
    x: 50 + 50,
    y: 0,
    yVelocity: 0,
    isJumping: false,
    jumpCD_InMiliSeconds: 5000,
    jumpForce: 10,

    start: function () {
        this.x = playerSpawnX;
        this.y = playerSpawnY;


    },

    useGravity: function () {
        this.yVelocity += GRAVITY;
        this.y += this.yVelocity;
    },

    update: function () {
        ctx.fillStyle = "#d6c41c";
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
        ctx.strokeStyle = "black";
        ctx.stroke();
        ctx.fill();
        ctx.closePath();
        this.useGravity();
        //console.log("isJumping = " + this.isJumping);
    },

    jump: function () {
        if (!this.isJumping) {
            this.isJumping = true;

            this.yVelocity -= this.jumpForce;
            setTimeout(this.enableJump(), 5000);
        }
    },

    enableJump: function () {
        this.isJumping = false;
    }
}

function Pipe(y, height) {
    this.index = -1;
    this.width = 100;
    this.height = height;
    this.color = "#28bf65"; //green
    this.x = canvasWidth;
    this.y = y;
    this.moveSpeed = 3;

    this.start = function start() {
        
    }

    this.update = function update() {
        ctx.fillStyle = "#28bf65";
        ctx.fillRect(this.x, this.y, this.width, this.height);
        this.x -= this.moveSpeed;

        if (this.x <= -this.width) {
            if (bottomPipes[this.index] == this) {
                bottomPipes.splice(this.index, 1);
            }
        }
    }
}

var pipesSpawner = {
    timeBetweenSpawns: 60,
    start: function () {
        //criar timer de spawn

    },

    update: function () {
        bottomPipes.forEach(pipe => {
            pipe.update();
        });

        if (this.timeBetweenSpawns <= 0) {
            this.timeBetweenSpawns = 100 + (Math.floor(200 * Math.random()));
            this.createStage();
        }
        else {
            this.timeBetweenSpawns--;
        }
    },

    createStage: function () {
        var tunelPercentage = (100 * TUNEL_HEIGHT) / canvasHeight;

        var topPipePercentage = 20 + Math.floor(30 * Math.random()) //min 20%, max 50%
        var topHeight = canvasHeight * topPipePercentage / 100;

        var bottomPipePercentage = 100 - tunelPercentage - topPipePercentage;
        var bottomHeight = canvasHeight * bottomPipePercentage / 100;

        this.createBottomPipe(bottomHeight);
        this.createTopPipe(topHeight);

    },

    createTopPipe: function (height) {
        var newPipe = new Pipe(0, height);
        newPipe.index = bottomPipes.length;
        console.log("pipe index " + newPipe.index);
        newPipe.start();
        bottomPipes.push(newPipe);
    },

    createBottomPipe: function (height) {
        var newPipe = new Pipe(canvasHeight - height, height);
        newPipe.index = bottomPipes.length;
        console.log("pipe index " + newPipe.index);
        newPipe.start();
        bottomPipes.push(newPipe);
    }


}

function drawBackground() {
    ctx.fillStyle = "#77d9fc";
    ctx.fillRect(0, 0, canvasWidth, canvasHeight);
}

function handleInput(event) {
    switch (event.keyCode) {
        case INPUT.space:
            player.jump();
            break;
    }
}

function start() {
    canvas = document.getElementById("gameCanvas");
    canvasWidth = canvas.clientWidth;
    canvasHeight = canvas.clientHeight;

    currentGameState = GAME_STATES.notStarted;

    ctx = canvas.getContext("2d");
    playerSpawnX = 100;
    playerSpawnY = canvasHeight / 2;

    player.start();
    pipesSpawner.start();

    //Always in the end of this method
    updateInterval = setInterval(update, 20);
}

function update() {    
    clearCanvas();
    drawBackground();
    
    if(currentGameState == GAME_STATES.playing) {
        player.update();
        pipesSpawner.update();
    }
}

function clearCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}





start();


